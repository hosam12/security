<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\AdminAuthController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('cms.admin.parent');
})->middleware('auth:user');

Route::prefix('/cms/admin/')->middleware('auth:user')->group(function () {

    Route::resource('user', UserController::class);
    Route::resource('file', FileController::class);
    Route::get('logout', [AdminAuthController::class, 'logout'])->name('admin.logout');

    Route::get('password/reset', [AdminAuthController::class, 'showResetPasswordView'])->name('admin.password_reset_view');
    Route::post('password/reset', [AdminAuthController::class, 'resetPassword'])->name('admin.password_reset');

    Route::get('/', [AdminController::class, 'dashbord'])->name('admin.dashbord');
    Route::get('login', [AdminAuthController::class, 'showLoginView'])->name('user.login.view');
        Route::get('{id}/user/edit-permissions', [UserController::class, 'editPermissions'])->name('user.edit-permissions');
    Route::post('per/{id}/user/update-permissions', [UserController::class, 'updatePermissions'])->name('users.update-permissions');
    //


});


Route::prefix('/cms/admin/')->middleware('auth:user')->group(function () {
    Route::resource('permissions', PermissionController::class);
        Route::resource('roles', RoleController::class);

          Route::get('roles/{id}/edit-permissions', [RoleController::class, 'editRolePermissions'])->name('roles.edit-permissions');
    Route::post('roles/{id}/update-permissions', [RoleController::class, 'updateRolePermissions'])->name('roles.update-permissions');
     Route::get('{id}/user/edit-roles', [UserController::class, 'editRoles'])->name('user.edit-roles');
    Route::post('per/{id}/user/update-roles', [UserController::class, 'updateRoles'])->name('users.update-roles');

        Route::get('{id}/user/edit-roles', [UserController::class, 'editRoles'])->name('user.edit-roles');
    Route::post('per/{id}/user/update-roles', [UserController::class, 'updateRoles'])->name('users.update-roles');



});



Route::prefix('cms/admin')->namespace('Auth')->group(function () {


    Route::get('login', [AdminAuthController::class, 'showLoginView'])->name('user.login.view');
    Route::post('post/login', [AdminAuthController::class, 'login'])->name('user.login.store');
});
