<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
// use Spatie\Permission\Contracts\Permission;
use Spatie\Permission\Models\Permission;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Permission::create(['name' => 'create-user', 'guard_name' => 'user', 'name_ar' => 'create user', 'created_at' => now(), 'updated_at' => now()]);
        // Permission::create(['name' => 'update-user', 'guard_name' => 'user', 'name_ar' => 'update user', 'created_at' => now(), 'updated_at' => now()]);
        // Permission::create(['name' => 'delete-user', 'guard_name' => 'user', 'name_ar' => 'delete user', 'created_at' => now(), 'updated_at' => now()]);
        // Permission::create(['name' => 'show-users', 'guard_name' => 'user', 'name_ar' => 'show user ', 'created_at' => now(), 'updated_at' => now()]);



       Permission::create(['name' => 'create-file', 'guard_name' => 'user', 'name_ar' => 'create file', 'created_at' => now(), 'updated_at' => now()]);
        Permission::create(['name' => 'update-file', 'guard_name' => 'user', 'name_ar' => 'update file', 'created_at' => now(), 'updated_at' => now()]);
        Permission::create(['name' => 'delete-file', 'guard_name' => 'user', 'name_ar' => 'delete file', 'created_at' => now(), 'updated_at' => now()]);
        Permission::create(['name' => 'show-file', 'guard_name' => 'user', 'name_ar' => 'show file ', 'created_at' => now(), 'updated_at' => now()]);

    }
}
