@extends('cms.admin.parent')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Users</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Security - Users</h3>
                            <a href="{{route('user.create')}}" class="btn btn-sm btn-info float-right">Create New
                                User</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Permission</th>

                                    <th>Roles</th>





                                    <th>Gender</th>

                                    <th>Settings</th>
                                </tr>
                                </thead>
                                <tbody>
                                <span hidden>{{$count = 0}}</span>
                                @foreach($users as $user)
                                    <tr>
                                        <td><span class="badge badge-info">{{++$count}}</span></td>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                                                  <td>
                                                                      <a class="btn btn-primary btn-sm" href="{{route('user.edit-permissions',[$user->id])}}">
                                                                          <i class="fas fa-sign">
                                                                          </i>
                                                                          Permission
                                                                      </a>
                                                                  </td>


                                                                  <td>
                                                                      <a class="btn btn-danger btn-sm" href="{{route('user.edit-roles',[$user->id])}}">

                                                                          <i class="fas fa-signature">
                                                                          </i>
                                                                          Roles
                                                                      </a>
                                                                  </td>


                                        <td>
                                            @if($user->gender == 'Male')
                                                <span class="badge badge-info">{{$user->gender}}</span>
                                            @else
                                                <span class="badge badge-warning">{{$user->gender}}</span>
                                            @endif
                                        </td>


                                        </td>

                                        <td>
                                            <a href="#" onclick="confirmDelete(this, '{{$user->id}}')"
                                               class="btn btn-xs btn-danger" style="color: white;">Delete</a>
                                            <a href="{{route('user.edit',[$user->id])}}"
                                               class="btn btn-xs btn-info" style="color: white;">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                            <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
  <th>Permission</th>

  <th>Roles</th>


                                    <th>Gender</th>

                                    <th>Settings</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{asset('cms/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('cms/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>

        function confirmDelete(app, id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    deleteUser(app, id)
                }
            })
        }

        function deleteUser(app, id) {
            axios.delete('/cms/admin/user/' + id)
                .then(function (response) {
                    // handle success (Status Code: 200)
                    console.log(response);
                    console.log(response.data);
                    showMessage(response.data);
                    app.closest('tr').remove();
                })
                .catch(function (error) {
                    // handle error (Status Code: 400)
                    console.log(error);
                    console.log(error.response.data);
                    showMessage(error.response.data);
                })
                .then(function () {
                    // always executed
                });
        }

        function showMessage(data) {
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.title,
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>
@endsection
