@extends('cms.admin.parent')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>File - Create</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}">Home</a></li>
                            <li class="breadcrumb-item active">Create File</li>

                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create File</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form role="form" method="post" action="{{route('file.store')}}">
                                @csrf

                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" role="alert">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    @if (session()->has('message'))
                                    <div class="alert {{session()->get('status')}} alert-dismissible fade show" role="alert">
                                        <span> {{ session()->get('message') }}<span>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                    @endif

   <div class="form-group">
       <label for="exampleInputPassword1">Title file</label>
       <input  name="title" value="{{old('title')}}" type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter  Titles">
   </div>


                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Contenet</label>
                                        <textarea col="10" id="content" row="10" name="content" type="text" placeholder="Enter Content" class="textarea form-control no-resize summernote">{{old('content')}}</textarea>



                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')

<script src="https://cdn.jsdelivr.net/npm/trumbowyg@2"></script>
<link href="https://cdn.jsdelivr.net/npm/trumbowyg@2/dist/ui/trumbowyg.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/vue-trumbowyg@3"></script>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://unpkg.com/vee-validate@<3.0.0"></script>

<script>

$(document).ready(function() {

$('#content').summernote({



height: 300,

});



});

</script>
@endsection
