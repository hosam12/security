<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Support\Arr;

use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }


      protected function unauthenticated($request, AuthenticationException $exception)
    {

        // if ($request->expectsJson()) {
        //     dd(45);
        //     if($request->header('lang')=='en'){
        //         app()->setlocale('en');
        //     }
        //     return response()->json(['message' => Lang::get('messages.UNAUTHORISED')], 401);
        // }

        $guard = Arr::get($exception->guards(), 0);


         if ($guard == 'user'){
            $login = 'user.login.view';

        }


        return redirect()->guest(route($login));
    }
}
