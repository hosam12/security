<?php

namespace App\Http\Requests;

use GuzzleHttp\Psr7\Message;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'name'=>'required',
                        'email'=>'required|email|unique:users,email',
                        'password'=>'required|min:6',
                        'confirmedpassword'=>'same:password',
                        'gender'=>'required|in:Male,Female'
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {

                    if ($this->post('password')) {
                        return [
                            'name'=>'required',

                        'email'=>'required|email|unique:users,email,'.$this->get('id'),
                        'password'=>'required',
                          'gender'=>'required|in:Male,Female'
                        ];
                    } else {
                        return [
                         'name'=>'required',

                        'email'=>'required|email|unique:users,email,'.$this->get('id'),

                          'gender'=>'required|in:Male,Female'
                        ];
                    }
                }
                }




    }

       public function messages()
    {


        return [

            'name.required'=>'The Name is required',
            'gender.required'=>'The Gender is required',
            'gender.in'=>'The Sekect Gender is Error',

            'email.required'=>'Email is required',
            'email.unique'=>'Email is aleardy taken',
            'email.email'=>'Please check the entered email',
            'password.required'=>'Password is required',
            'password.min'=>'password should be at least 6 characters on the',
            'confirmedpassword.required'=>'Confirmed Password is required ',
            'confirmedpassword.same'=>'Confirmed Password must be same of password ',



        ];



    }
}
