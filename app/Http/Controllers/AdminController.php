<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public function dashbord(){

        if(Auth::user()->type=='admin'){
            $users=User::where('type','user')->get();
        return view('cms.admin.users.index',['users'=>$users]);

        }

        return Redirect()->route('file.index');
    }
}
