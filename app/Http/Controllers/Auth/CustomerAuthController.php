<?php

namespace App\Http\Controllers\Auth;

// use App\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\Mail\CustomerPasswordReset;
use App\Mail\UserPasswordReset;
use App\Models\Login;
use App\Models\Customer;
use Carbon\Carbon;
//
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class CustomerAuthController extends Controller
{
    //
    // use AuthenticatesUsers;

    protected $guardName = 'admin';
    protected $maxAttempts = 4;
    protected $decayMinutes = 2;

    public function __construct()
    {

        $this->middleware('guest:customer')->except(['logout']);
        // $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginView()
    {
        return view('cms.site._signInModal');
    }

    public function showSignUpView()
    {
        return view('cms.site._signUpModal');
    }

       public function signUp(CustomerRequest $request){

        $request->request->add(['email_Customer' =>$request->email,'password_Customer'=>$request->password]);
        $customer=new Customer();
        $customer->firstName=$request->firstName;
        $customer->lastName=$request->lastName;
        $customer->email=$request->email;
        $customer->password=Hash::make($request->password);

        $customer->save();
        if($customer){
        $request->request->add(['email_Customer' =>$request->email,'password_Customer'=>$request->password]);
            $this->login($request);
        return ['status'=>'done'];
        }

        else{
            return ['status'=>'error'];
        }



    }


    public function login(Request $request)
    {




        if (app()->getLocale() == 'en') {
            $Message = [
                'email_Customer.required' => 'Email field is required',
                'password_Customer.required' => 'Password field is required',

                'email_Customer.email' => 'Please check the entered email',
                'email_Customer.exists' => "This email doesn't exist",
            ];
        } else {
            $Message = [
                'email_Customer.required' => 'البريد الإكتروني فارغ',
                'password_Customer.required' => 'كلمة المرور فارغة',

                'email_Customer.email' => 'الرجاء التأكد من البريد الإكتروني ',
                'email_Customer.exists' => "البريد الإلكتروني غير موجود",
            ];
        }
        $request->validate(
            [
                'email_Customer' => 'required|email:rfc,dns|string|exists:customers,email',
                'password_Customer' => 'required|string'], $Message

        );



       // $rememberMe = $request->remember_me == 'on' ? true : false;

        $credentials = [
            'email' => $request->email_Customer,
            'password' => $request->password_Customer,
        ];


        if (Auth::guard('customer')->attempt($credentials)) {
            // return 55;


            $customer = Auth::guard('customer')->user();
                       return response()->json(['status' =>'done'],200);

        } else {

      $Message='';
      $Message=      app()->getLocale()=='en' ? 'The password you’ve entered is incorrect' : 'كلمة السر خاطئة';

            return response()->json(['password_CustomerNot' => $Message],400);


        }
    }

    public function showResetPasswordView($resetToken)
    {
            // return view('cms.site._reset',['resetToken'=>$resetToken]);

          $customer = Customer::where('resetToken',$resetToken)->first();
        if($customer){

        return view('cms.site._reset',['resetToken'=>$resetToken]);

        }
        else
     return   'no';



    }

    public function resetPassword(Request $request)
    {
        // return 5;
        $Message = [];
        if (app()->getLocale() == 'en') {

            $Message = [


                'new_password.required' => 'The new password is Empty ',
                'new_password_confirmation.same' => 'Confirmed Password must be same of password ',
                'new_password_confirmation.required'=>'Confirmed Password is Empty',
                'new_password.min'=>'The new  password should be at least 6 characters on the',


            ];
        } else {
            $Message = [


                 'new_password.required' => 'كلمة السر الجديدة فارغة',
                'new_password_confirmation.same' => 'الكلمتان غير متشابهتان',
                'new_password_confirmation.required'=>'كلمة التأكيد فارغة',
                'new_password.min'=>'كلمة السر أقل من 6 حروف',


            ];
        }

        $request->validate([
            'new_password' => 'required|min:6|string',
            'new_password_confirmation' => 'required|string|same:new_password',
        ],$Message);

        $customer = Customer::where('resetToken',$request->get('resetToken'))->first();
        $customer->password = Hash::make($request->get('new_password'));
        $customer->resetToken=null;
        $isSaved = $customer->save();
        return 'success';

    }

    public function logout(Request $request)
    {



        Auth::guard('customer')->logout();

        return 'logout';

    }



    public function requestNewPassword(Request $request)
    {
//   return 500;
        // dd(app()->getLocale());
        $Message=[];
      if (app()->getLocale() == 'en') {
            $Message=[

            'email_forgot.required' => 'Email  field is required',
            'email_forgot.exists' => " Email doesn't exist",
            'email_forgot.email'=>'Please check the email',

            ];
        }
        else {

            $Message=[

            'email_forgot.required' => 'البريد الإلكتروني فارغ',
            'email_forgot.exists' => 'الإيميل غير موجود',
            'email_forgot.email'=>'الرجاءالتأكد من البريد الإكتروني',

            ];
        }
        $request->validate([
            'email_forgot' => 'required|exists:customers,email|email',
        ], $Message);
        // return 55;

        $newPassword = Str::random(8);

        $customer = Customer::where('email', $request->get('email_forgot'))->first();
        $customer->password = Hash::make($newPassword);

        $customer->resetToken=Str::random(10);
        $isSaved = $customer->save();

        if ($isSaved) {
            $this->sendResetPasswordEmail($customer, $newPassword);
            // return 55555555555;
            // return redirect()->route('customer.login.view');
        } else {
        }
    }

    private function sendResetPasswordEmail(Customer $customer, $newPassword)
    {
        Mail::queue(new CustomerPasswordReset($customer, $newPassword));
    }


}
