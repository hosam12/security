<?php

namespace App\Http\Controllers\Auth;

// use App\Admin;
use App\Http\Controllers\Controller;

// use App\Models\Admin;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AdminAuthController extends Controller
{
    //
    // use AuthenticatesUsers;

    protected $guardName = 'admin';
    protected $maxAttempts = 4;
    protected $decayMinutes = 2;

    public function __construct()
    {

        $this->middleware('guest:user')->except(['logout','showResetPasswordView','resetPassword']);
        // $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginView(){
        return view('cms.admin.auth.login');
    }

    public function login(Request $request){
    //    dd(Admin::where('email',$request->email)->first());
        $request->validate([
            // 'email'=>'required|string|exists:admins,email',
            'email'=>'required|string|exists:users,email',

            'password'=>'required|string|min:3',
            'remember_me'=>'string|in:on'
        ],
    [
        'email.required'=>'The Email The field is required',
          'password.required'=>'Please enter a password',

        'email.email'=>'Please check the entered email',
        'email.exists'=>"This email doesn't exist",

    ]
    );



        $rememberMe = $request->remember_me == 'on' ? true : false;

        $credentials = [
            'email'=>$request->email,
            'password'=>$request->password,
        ];

        if (Auth::guard('user')->attempt($credentials, $rememberMe)){

            $user = Auth::guard('user')->user();



                return redirect()->route('admin.dashbord');










        }else{

            // return Redirect::back()->withErrors(['msg', 'The Message']);
            return redirect()->back()->withInput()->withErrors([ 'The password you’ve entered is incorrect']);
        }
    }


    public function showResetPasswordView(){
        // dd(45);
        return view('cms.admin.auth.reset_password');
    }

    public function resetPassword(Request $request)
    {

        // return 55;

        $request->validate([
            'current_password' => 'required|string|password:user',
            'new_password' => 'required|min:6|string',
            'new_password_confirmation' => 'same:new_password'
        ],[

              'current_password.required'=>'The Current Password field is required',
              'new_password.min'=>'The new  password should be at least 6 characters on the',
            'current_password.password'=>'The current password is wrong',
            'new_password.required'=>'The New password field is required',
            'new_password_confirmation.required'=>'Confirmation Password field is required',
            'new_password_confirmation.same'=>"The Password and Confirmation password doesn't match",



            ]);

       $user = Auth::user();
        $user->password = Hash::make($request->get('new_password'));

        $isSaved = $user->save();
        if ($isSaved) {
            return response()->json(['icon' => 'success', 'title' => 'Edit successfully'], 200);
        } else {
            return response()->json(['icon' => 'success', 'title' => 'Edit Faild!'], 400);
        }
    }

    public function logout(Request $request){
        // dd(45);
        Auth::guard('user')->logout();
        $request->session()->invalidate();
        return redirect()->guest(route('user.login.view'));
    }





}
