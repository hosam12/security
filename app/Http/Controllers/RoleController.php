<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
     public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $type = Auth::user()->type;
            if ($type == 'user') {
                Auth::guard('user')->logout();
                $request->session()->invalidate();
                return redirect()->guest(route('user.login.view'));
            }
            return $next($request);
        });

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        //
       if (Auth::user()->type=='admin') {
            $roles = Role::all();
            return view('cms.admin.spatie.roles.index', ['roles' => $roles]);

        }
              return view('cms.admin.blocked');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
          if (Auth::user()->type=='admin') {

            return view('cms.admin.spatie.roles.create');


    }
              return view('cms.admin.blocked');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
         if (Auth::user()->type=='admin') {
        $request->validate([

            'name' => 'required|String|min:3|max:45',

        ], [
            'name.required' => 'Role name is required',

            // 'guard_name.in'=>'يج'

        ]);

        $role = new Role();
        $role->name = $request->get('name');
        $role->guard_name = 'user';

        $isSaved = $role->save();
        if ($isSaved) {
            return response()->json(['icon' => 'success', 'title' => 'Create Successfuly'], 200);
        } else {
            return response()->json(['icon' => 'success', 'title' => 'Create Failed'], 400);
        }
    }
                  return view('cms.admin.spatie.blocked');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        // dd(45);
         if (Auth::user()->type=='admin') {


            $role = Role::find($id);
            return view('cms.admin.spatie.roles.edit', ['role' => $role]);


    }
                  return view('cms.admin.blocked');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
          if (Auth::user()->type=='admin') {
        $request->request->add(['id' => $id]);
        $request->validate([
            'id' => 'required|exists:roles,id|integer',
            'name' => 'required|String|min:5|max:45',

        ], [
               'name.required' => 'Role name is required',

        ]);

        $role = Role::find($id);
        $role->name = $request->get('name');
        $role->guard_name = 'user';
        $isUpdated = $role->save();
        if ($isUpdated) {
            return response()->json(['icon' => 'success', 'title' => 'Update Successfuly'], 200);
        } else {
            return response()->json(['icon' => 'success', 'title' => 'Update Failed'], 400);
        }
    }
                      return view('cms.admin.blocked');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
            if (Auth::user()->type=='admin') {
        $isDestroyed = false;

            $isDestroyed = Role::destroy($id);


        if ($isDestroyed) {
            return response()->json([
                'icon' => 'success',
                'title' => 'Delete Successfuly',
            ], 200);
        } else {
            return response()->json([
                'icon' => 'error',
                'title' => 'Delete Failed',
            ], 400);
        }
    }
     return response()->json([
                'icon' => 'error',
                'title' => 'Delete Failed',
            ], 400);
    }

    public function editRolePermissions(Request $request, $id)
    {
        if (Auth::user()->type=='admin') {
        $role = Role::find($id);
        $rolePermissions = $role->permissions()->get();
        $permissions = Permission::where('guard_name', $role->guard_name)->get();
        return view('cms.admin.spatie.roles.edit-role-permissions', [
            'role' => $role,
            'permissions' => $permissions,
            'rolePermissions' => $rolePermissions]);
        }
         return view('cms.admin.blocked');
    }

    public function updateRolePermissions(Request $request, $id)
    {
         if (Auth::user()->type=='admin') {
        $request->request->add(['id' => $id]);
        $request->validate([
            'id' => 'required|exists:roles,id',
            'permissions' => 'array',
        ]);

        $role = Role::find($request->get('id'));
        $permissions = Permission::findMany($request->permissions);
        $isSynced = $role->syncPermissions($permissions);
        if ($isSynced) {
            return response()->json(['icon' => 'success', 'title' => 'Update Successfuly'], 200);
        } else {
            return response()->json(['icon' => 'Failed', 'title' => 'Update Failed'], 400);
        }
    }
}
}
