<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $type = Auth::user()->type;
            if ($type == 'user') {
                Auth::guard('user')->logout();
                $request->session()->invalidate();
                return redirect()->guest(route('user.login.view'));
            }
            return $next($request);
        });

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // dd(5);
        if ( Auth::user()->type=='admin') {

            // if (auth('admin')->user()->hasPermissionTo('read-permissions')) {
            $permissions = Permission::all();
            return view('cms.admin.spatie.permissions.index', ['permissions' => $permissions]);
            // }

        }
        return view('cms.admin.blocked');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        //
        if (Auth::user()->type=='admin' ) {
            return view('cms.admin.spatie.permissions.create');
            // }
        }

        return view('cms.admin.blocked');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        //
        // $request->validate([
        //     'name' => 'required|string|min:5|max:45',
        //     'guard_name' => 'string|in:admin,student',
        // ]);

        // $permission = new Permission();
        // $permission->name = $request->get('name');
        // $permission->guard_name = $request->get('guard_name');
        // $isSaved = $permission->save();
        // if ($isSaved) {
        //     return response()->json(['icon' => 'success', 'title' => 'Permission created successfully'], 200);
        // } else {
        //     return response()->json(['icon' => 'success', 'title' => 'Permission create failed!'], 400);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        //
        if (Auth::user()->type=='admin') {
            // if (auth('admin')->user()->hasPermissionTo('update-permission')) {
            $permission = Permission::find($id);
            return view('cms.admin.spatie.permissions.edit', ['permission' => $permission]);
            // }
        }
        return view('cms.admin.blocked');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->type=='admin') {
            $request->request->add(['id' => $id]);
            $request->validate([
                'id' => 'required|exists:permissions,id|integer',
                'name_ar' => 'required|string|min:5|max:45',
                // 'guard_name' => 'string|in:user,employe',
            ], [
                'name_ar.required' => 'اسم الإذن فارغ',
                'name_ar.min' => 'عدد الحروف أقل من 5',
            ]);

            $permission = Permission::find($id);
            $permission->name_ar = $request->get('name_ar');
            // $permission->guard_name = $request->get('guard_name');
            $isUpdated = $permission->save();
            if ($isUpdated) {
                return response()->json(['icon' => 'success', 'title' => 'تم التعديل بنجاح'], 200);
            } else {
                return response()->json(['icon' => 'Failed', 'title' => 'فشل التعديل'], 400);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if (Auth::user()->type=='admin') {
            $isDestroyed = false;
            if (auth('admin')->check()) {
                if (auth('admin')->user()->hasPermissionTo('delete-permission')) {
                    $isDestroyed = Permission::destroy($id);
                }
            }
            if ($isDestroyed) {
                return response()->json([
                    'icon' => 'success',
                    'title' => 'Permission deleted successfully',
                ], 200);
            } else {
                return response()->json([
                    'icon' => 'error',
                    'title' => 'Permission delete failed',
                ], 400);
            }
        }
    }
}
